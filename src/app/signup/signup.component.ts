import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  signupForm = new FormGroup({
    nome: new FormControl('', [Validators.maxLength(45), Validators.required]),
    nif: new FormControl('', [
      Validators.pattern(/^\d{9}$/),
      Validators.required,
    ]),
    cod_postal: new FormControl('', [
      Validators.pattern(/^\d{4}-\d{3}$/),
      Validators.required,
    ]),
    morada: new FormControl('', [
      Validators.maxLength(45),
      Validators.required,
    ]),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(
        /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/
      ),
    ]),
    confirmPassword: new FormControl('', [
      Validators.required,
      this.validateConfirmPassword(),
    ]),
  });
  hidePassword = true;

  constructor(private authService: AuthService, private router: Router) {}

  validateConfirmPassword() {
    return (control: AbstractControl) => {
      if (!(control.value && this.signupForm.value.password)) {
        return { isSame: false };
      }
      return null;
    };
  }

  onSubmit() {
    if (this.signupForm.valid && this.signupForm.dirty) {
      this.authService
        .signup(
          this.signupForm.value.email as string,
          this.signupForm.value.password as string,
          this.signupForm.value.nome as string,
          this.signupForm.value.morada as string,
          this.signupForm.value.nif as string,
          this.signupForm.value.cod_postal as string
        )
        .subscribe(() => this.router.navigate(['/login']));
    }
  }
}
