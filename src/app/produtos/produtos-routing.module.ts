import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProdutosComponent } from './produtos.component';

const routes: Routes = [
  {
    path: '',
    component: ProdutosComponent,
  },
  {
    path: 'criar',
    loadChildren: () =>
      import('../produtos/criar-produto/criar-produto.module').then(
        (m) => m.CriarProdutoModule
      ),
  },
  {
    path: 'editar',

    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/protected/produtos',
      },
      {
        path: ':id',
        loadChildren: () =>
          import('../produtos/editar-produto/editar-produto.module').then(
            (m) => m.EditarProdutoModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProdutosRoutingModule {}
