import { Component, ViewChild, OnInit } from '@angular/core';
import { Produtos, ProdutosTable } from '../models/produtos';
import { MatTableDataSource } from '@angular/material/table';
import { ProdutosService } from '../services/produtos.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {
  catchError,
  map,
  startWith,
  switchMap,
  mergeWith,
} from 'rxjs/operators';
import { of, pipe } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../common/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss'],
})
export class ProdutosComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nome', 'preco', 'acoes'];

  empTable: ProdutosTable;

  totalData: number;
  ProdutosData: Produtos[];
  pageSizes = [5, 10];
  dataSource = new MatTableDataSource<Produtos>();
  selectedRow: Produtos;
  isLoading = false;

  constructor(
    private produtosService: ProdutosService,
    public dialog: MatDialog
  ) {}

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.refreshData();
  }

  refreshData() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    this.sort.sortChange
      .pipe(
        mergeWith(this.paginator.page),
        startWith({}),
        switchMap(() => {
          this.isLoading = true;
          return this.produtosService
            .getPaginatedProdutos(
              this.paginator.pageIndex + 1,
              this.paginator.pageSize,
              this.sort.active,
              this.sort.direction
            )
            .pipe(catchError(() => of(null)));
        }),
        map((data) => {
          this.isLoading = false;
          if (data === null) {
            return [];
          }

          this.totalData = data.total;
          return data.data;
        })
      )
      .subscribe((data) => {
        this.ProdutosData = data;
        console.log(this.ProdutosData);

        this.dataSource = new MatTableDataSource(this.ProdutosData);
      });
  }

  // * Apenas para debug
  logClickedRow(row: Produtos) {
    console.log(row);
    this.selectedRow = row;
  }

  ngOnInit(): void {
    this.isLoading = true;
  }

  openConfirmationDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: { message: 'Tem certeza que deseja remover este produto?' },
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.produtosService.deleteProduto(this.selectedRow.id).subscribe({
          next: (res) => {
            this.refreshData();
          },
        });
      }
    });
  }
}
