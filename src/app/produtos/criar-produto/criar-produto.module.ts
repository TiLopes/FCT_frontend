import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriarProdutoRoutingModule } from './criar-produto-routing.module';
import { CriarProdutoComponent } from './criar-produto.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NgxCurrencyDirective,
  NgxCurrencyInputMode,
  provideEnvironmentNgxCurrency,
} from 'ngx-currency';

@NgModule({
  declarations: [CriarProdutoComponent],
  providers: [
    provideEnvironmentNgxCurrency({
      align: 'left',
      // allowNegative: true,
      allowZero: true,
      decimal: ',',
      precision: 2,
      prefix: '',
      suffix: ' €',
      thousands: '.',
      nullable: false,
      min: null,
      max: null,
      inputMode: NgxCurrencyInputMode.Financial,
    }),
  ],
  imports: [
    CommonModule,
    CriarProdutoRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    NgxCurrencyDirective,
  ],
})
export class CriarProdutoModule {}
