import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdutosService } from 'src/app/services/produtos.service';

@Component({
  selector: 'app-criar-produto',
  templateUrl: './criar-produto.component.html',
  styleUrls: ['./criar-produto.component.scss'],
})
export class CriarProdutoComponent {
  criarProdutoForm = new FormGroup({
    nome: new FormControl('', [Validators.required]),
    preco: new FormControl('', [Validators.required]),
  });

  constructor(
    private produtosService: ProdutosService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {}

  onSubmit() {
    if (this.criarProdutoForm.invalid) {
      return;
    }

    this.produtosService
      .addProduto({
        nome: this.criarProdutoForm.get('nome')?.value!,
        preco: Number(this.criarProdutoForm.get('preco')?.value!),
      })
      .subscribe({
        next: (res) => {
          this.router.navigate(['..'], { relativeTo: this.route });
          this.snackBar.open('Criado com sucesso', undefined, {
            panelClass: ['snackbar-success', 'centered'],
            duration: 3 * 1000,
          });
          console.log(res);
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
