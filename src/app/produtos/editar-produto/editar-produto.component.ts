import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdutosService } from 'src/app/services/produtos.service';

@Component({
  selector: 'app-editar-produto',
  templateUrl: './editar-produto.component.html',
  styleUrls: ['./editar-produto.component.scss'],
})
export class EditarProdutoComponent {
  editarProdutoForm = new FormGroup({
    id: new FormControl({ value: '', disabled: true }),
    nome: new FormControl('', [Validators.required]),
    preco: new FormControl('', [Validators.required]),
  });

  id_produto: string;

  constructor(
    private produtosService: ProdutosService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.id_produto = this.route.snapshot.paramMap.get('id')!;

    this.produtosService.getProduto(this.id_produto).subscribe({
      next: (res) => {
        console.log(res);
        this.editarProdutoForm.get('id')?.setValue(res.produto.id.toString());
        this.editarProdutoForm.get('nome')?.setValue(res.produto.nome);
        this.editarProdutoForm
          .get('preco')
          ?.setValue(res.produto.preco.toString());
      },
      error: (err) => {
        console.log(err);
        this.router.navigate(['..'], { relativeTo: this.route });
      },
    });
  }

  onSubmit() {
    if (this.editarProdutoForm.invalid) {
      return;
    }

    this.produtosService
      .editProduto({
        id: Number(this.id_produto),
        nome: this.editarProdutoForm.get('nome')?.value!,
        preco: Number(this.editarProdutoForm.get('preco')?.value!),
      })
      .subscribe({
        next: (res) => {
          this.router.navigate(['..'], { relativeTo: this.route });
        },
        error: (err) => {
          console.log(err);
          this.snackBar.open('Ocorreu um erro. Tente novamente', undefined, {
            panelClass: ['snackbar-error', 'centered'],
            duration: 3 * 1000,
          });
        },
      });
  }
}
