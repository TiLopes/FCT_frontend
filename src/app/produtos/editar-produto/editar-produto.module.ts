import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarProdutoRoutingModule } from './editar-produto-routing.module';
import { EditarProdutoComponent } from './editar-produto.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [EditarProdutoComponent],
  imports: [
    CommonModule,
    EditarProdutoRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
  ],
})
export class EditarProdutoModule {}
