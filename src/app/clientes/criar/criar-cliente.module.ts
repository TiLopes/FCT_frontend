import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriarClienteRoutingModule } from './criar-cliente-routing.module';
import { CriarClienteComponent } from './criar-cliente.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [CriarClienteComponent],
  imports: [
    CommonModule,
    CriarClienteRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
  ],
})
export class CriarClienteModule {}
