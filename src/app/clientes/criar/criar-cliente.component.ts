import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClientesService } from 'src/app/services/clientes.service';

@Component({
  selector: 'app-criar-cliente',
  templateUrl: './criar-cliente.component.html',
  styleUrls: ['./criar-cliente.component.scss'],
})
export class CriarClienteComponent {
  criarClienteForm = new FormGroup({
    nome: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.email, Validators.required]),
    telefone: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\+\d{1,3}\s\d{6,14}$/),
    ]),
    nif: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\d{9}$/),
    ]),
    cod_postal: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\d{4}-\d{3}$/),
    ]),
    morada: new FormControl('', [Validators.required]),
  });

  constructor(
    private clientesService: ClientesService,
    private snackbar: MatSnackBar
  ) {}

  onSubmit() {
    if (this.criarClienteForm.invalid) {
      return;
    }

    this.clientesService
      .addCliente({
        nome: this.criarClienteForm.get('nome')?.value!,
        email: this.criarClienteForm.get('email')?.value!,
        telefone: this.criarClienteForm.get('telefone')?.value!,
        nif: Number(this.criarClienteForm.get('nif')?.value!),
        cod_postal: this.criarClienteForm.get('cod_postal')?.value!,
        morada: this.criarClienteForm.get('morada')?.value!,
      })
      .subscribe({
        next: (res) => {
          console.log(res);
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
