import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientesService } from 'src/app/services/clientes.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.scss'],
})
export class EditarClienteComponent implements OnInit {
  editarClienteForm = new FormGroup({
    id: new FormControl({ value: '', disabled: true }),
    nome: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.email, Validators.required]),
    telefone: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\+\d{1,3}\s\d{6,14}$/),
    ]),
    nif: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\d{9}$/),
    ]),
    cod_postal: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\d{4}-\d{3}$/),
    ]),
    morada: new FormControl('', [Validators.required]),
  });

  id_cliente: string;

  constructor(
    private clientesService: ClientesService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.id_cliente = this.route.snapshot.paramMap.get('id')!;

    this.clientesService.getCliente(this.id_cliente).subscribe({
      next: (res) => {
        console.log(res);
        this.editarClienteForm.get('id')?.setValue(res.cliente.id.toString());
        this.editarClienteForm.get('nome')?.setValue(res.cliente.nome);
        this.editarClienteForm.get('email')?.setValue(res.cliente.email);
        this.editarClienteForm.get('telefone')?.setValue(res.cliente.telefone);
        this.editarClienteForm.get('nif')?.setValue(res.cliente.nif.toString());
        this.editarClienteForm
          .get('cod_postal')
          ?.setValue(res.cliente.cod_postal);
        this.editarClienteForm.get('morada')?.setValue(res.cliente.morada);
      },
      error: (err) => {
        console.log(err);
        this.router.navigate(['..'], { relativeTo: this.route });
      },
    });
  }

  onSubmit() {
    if (this.editarClienteForm.invalid) {
      return;
    }

    this.clientesService
      .editCliente({
        id: Number(this.id_cliente),
        nome: this.editarClienteForm.get('nome')?.value!,
        email: this.editarClienteForm.get('email')?.value!,
        telefone: this.editarClienteForm.get('telefone')?.value!,
        nif: Number(this.editarClienteForm.get('nif')?.value!),
        cod_postal: this.editarClienteForm.get('cod_postal')?.value!,
        morada: this.editarClienteForm.get('morada')?.value!,
      })
      .subscribe({
        next: (res) => {
          this.router.navigate(['..'], { relativeTo: this.route });
        },
        error: (err) => {
          console.log(err);
          this.snackBar.open('Ocorreu um erro. Tente novamente', undefined, {
            panelClass: ['snackbar-error', 'centered'],
            duration: 3 * 1000,
          });
        },
      });
  }
}
