import { Component, ViewChild } from '@angular/core';
import { Clientes, ClientesTable } from '../models/clientes';
import { MatTableDataSource } from '@angular/material/table';
import { ClientesService } from '../services/clientes.service';
import { HttpClient } from '@angular/common/http';
import {
  catchError,
  map,
  startWith,
  switchMap,
  mergeWith,
} from 'rxjs/operators';
import { of, pipe } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../common/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss'],
})
export class ClientesComponent {
  displayedColumns: string[] = [
    'id',
    'nome',
    'email',
    'telefone',
    'nif',
    'cod_postal',
    'morada',
    'acoes',
  ];

  empTable: ClientesTable;

  totalData: number;
  ClientesData: Clientes[];
  pageSizes = [5, 10];
  dataSource = new MatTableDataSource<Clientes>();

  isLoading = false;

  constructor(
    private clientesService: ClientesService,
    private http: HttpClient,
    public dialog: MatDialog
  ) {}

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.refreshData();
  }

  refreshData() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    this.sort.sortChange
      .pipe(
        mergeWith(this.paginator.page),
        startWith({}),
        switchMap(() => {
          this.isLoading = true;
          return this.clientesService
            .getClientes(
              this.paginator.pageIndex + 1,
              this.paginator.pageSize,
              this.sort.active,
              this.sort.direction
            )
            .pipe(catchError(() => of(null)));
        }),
        map((data) => {
          // Flip flag to show that loading has finished.
          this.isLoading = false;
          if (data === null) {
            return [];
          }
          // Only refresh the result length if there is new data. In case of rate
          // limit errors, we do not want to reset the paginator to zero, as that
          // would prevent users from re-triggering requests.
          this.totalData = data.total;
          return data.data;
        })
      )
      .subscribe((data) => {
        this.ClientesData = data;
        console.log(this.ClientesData);

        this.dataSource = new MatTableDataSource(this.ClientesData);
      });
  }

  openConfirmationDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: { message: 'Tem certeza que deseja remover este produto?' },
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      console.log('The dialog was closed');
      console.log(result);
    });
  }

  // * Apenas para debug
  logClickedRow(row: Clientes) {
    console.log(row);
  }

  ngOnInit(): void {
    this.isLoading = true;
  }
}
