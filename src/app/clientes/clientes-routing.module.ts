import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './clientes.component';

const routes: Routes = [
  { path: '', component: ClientesComponent, pathMatch: 'full' },
  {
    path: 'criar',
    loadChildren: () =>
      import('./criar/criar-cliente.module').then((m) => m.CriarClienteModule),
  },
  {
    path: 'editar',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/protected/clientes',
      },
      {
        path: ':id',
        loadChildren: () =>
          import('./editar/editar-cliente.module').then(
            (m) => m.EditarClienteModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientesRoutingModule {}
