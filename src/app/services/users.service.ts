import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Users, UsersTable } from '../models/users';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getUsers(pageNumber: number, pageSize: number) {
    const url = `${environment.BASE_URL}/api/get/users?page=${pageNumber}&per_page=${pageSize}`;

    return this.http.get<UsersTable>(url);
  }

  getAllBasic$() {
    return this.http.get<{ users: Users[] }>(
      `${environment.BASE_URL}/api/get/users/basic`
    );
  }
}
