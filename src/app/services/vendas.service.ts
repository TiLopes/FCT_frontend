import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { Vendas, VendasTable } from '../models/vendas';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root',
})
export class VendasService {
  constructor(private http: HttpClient) {}

  getVendas(
    pageNumber: number,
    pageSize: number,
    sort: string,
    order: SortDirection
  ): Observable<VendasTable> {
    const url = `${environment.BASE_URL}/api/get/vendas?sort=${sort}&order=${order}&page=${pageNumber}&per_page=${pageSize}`;

    return this.http.get<VendasTable>(url);
  }

  getVenda(id: string) {
    return this.http.get<{ success: boolean; venda: Vendas }>(
      `${environment.BASE_URL}/api/get/venda/` + id
    );
  }
  // editVenda(venda: Vendas) {
  //   return this.http.post(
  //     '${environment.BASE_URL}/api/update/venda/' + venda.id,
  //     {
  //       nome: venda.nome,
  //       preco: venda.preco,
  //     }
  //   );
  // }

  addVenda({
    cliente,
    data_venda,
    preco,
    produtos,
  }: {
    cliente: number;
    data_venda: Date;
    preco: number;
    produtos: { id: number; quantidade: number }[];
  }) {
    return this.http.post(`${environment.BASE_URL}/api/add/venda`, {
      cliente,
      data_venda,
      preco,
      produtos,
    });
  }

  deleteVenda(id: number) {
    return this.http.post(`${environment.BASE_URL}/api/delete/venda/` + id, {
      id,
    });
  }
}
