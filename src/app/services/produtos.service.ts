import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { Produtos, ProdutosTable } from '../models/produtos';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root',
})
export class ProdutosService {
  constructor(private http: HttpClient) {}

  getPaginatedProdutos(
    pageNumber: number,
    pageSize: number,
    sort: string,
    order: SortDirection
  ): Observable<ProdutosTable> {
    const url = `${environment.BASE_URL}/api/get/produtos?sort=${sort}&order=${order}&page=${pageNumber}&per_page=${pageSize}`;

    return this.http.get<ProdutosTable>(url);
  }

  getAll() {
    return this.http.get<{ produtos: Produtos[] }>(
      `${environment.BASE_URL}/api/get/produtos`
    );
  }

  getProduto(id: string) {
    return this.http.get<{ success: boolean; produto: Produtos }>(
      `${environment.BASE_URL}/api/get/produto/` + id
    );
  }

  editProduto(produto: Produtos) {
    return this.http.post(
      `${environment.BASE_URL}/api/update/produto/` + produto.id,
      {
        nome: produto.nome,
        preco: produto.preco,
      }
    );
  }

  addProduto({
    nome,
    preco,
  }: {
    nome: string;

    preco: number;
  }) {
    return this.http.post(`${environment.BASE_URL}/api/add/produto`, {
      nome,
      preco,
    });
  }

  deleteProduto(id: number) {
    return this.http.post(`${environment.BASE_URL}/api/delete/produto/` + id, {
      id,
    });
  }
}
