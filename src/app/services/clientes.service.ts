import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Clientes, ClientesTable } from '../models/clientes';
import { SortDirection } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root',
})
export class ClientesService {
  constructor(private http: HttpClient) {}

  getClientes(
    pageNumber: number,
    pageSize: number,
    sort: string,
    order: SortDirection
  ): Observable<ClientesTable> {
    const url = `${environment.BASE_URL}/api/get/clientes?sort=${sort}&order=${order}&page=${pageNumber}&per_page=${pageSize}`;

    return this.http.get<ClientesTable>(url);
  }

  getAllBasic$() {
    return this.http.get<{ clientes: Clientes[] }>(
      `${environment.BASE_URL}/api/get/clientes/basic`
    );
  }

  getCliente(id: string) {
    return this.http.get<{ success: boolean; cliente: Clientes }>(
      `${environment.BASE_URL}/api/get/cliente/` + id
    );
  }

  editCliente(cliente: Clientes) {
    return this.http.post(
      `${environment.BASE_URL}/api/update/cliente/` + cliente.id,
      {
        nome: cliente.nome,
        email: cliente.email,
        telefone: cliente.telefone,
        nif: cliente.nif,
        cod_postal: cliente.cod_postal,
        morada: cliente.morada,
      }
    );
  }

  addCliente({
    nome,
    email,
    telefone,
    nif,
    cod_postal,
    morada,
  }: {
    nome: string;
    email: string;
    telefone: string;
    nif: number;
    cod_postal: string;
    morada: string;
  }) {
    return this.http.post(`${environment.BASE_URL}/api/add/cliente`, {
      nome,
      email,
      telefone,
      nif,
      cod_postal,
      morada,
    });
  }
}
