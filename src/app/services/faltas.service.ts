import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root',
})
export class FaltasService {
  constructor(private http: HttpClient) {}

  getFaltas$(data_inicio: Date, data_fim: Date) {
    return this.http.get<{ success: boolean; faltas: any[] }>(
      `${environment.BASE_URL}/api/get/faltas?data_inicio=${data_inicio}&data_fim=${data_fim}`
    );
  }

  justificarFaltas$(id: number) {
    return this.http.post(
      `${environment.BASE_URL}/api/justificar/falta/${id}`,
      {
        id,
      }
    );
  }

  marcarFalta$({
    id_utilizador,
    motivo,
    data_inicio,
    data_fim,
    dia_completo,
  }: {
    id_utilizador: number;
    motivo: string;
    data_inicio: Date;
    data_fim: Date;
    dia_completo: boolean;
  }) {
    return this.http.post(`${environment.BASE_URL}/api/add/falta`, {
      id_utilizador,
      motivo,
      data_fim,
      data_inicio,
      dia_completo,
    });
  }
}
