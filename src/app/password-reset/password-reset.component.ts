import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss'],
})
export class PasswordResetComponent implements OnInit {
  token: string | null;
  resetPasswordForm = new FormGroup({
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(
        /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/
      ),
    ]),
    confirmPassword: new FormControl('', [
      Validators.required,
      this.validateConfirmPassword(),
    ]),
  });

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
    private toast: MatSnackBar
  ) {}

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    if (!this.token) {
      this.router.navigateByUrl('/');
      return;
    }
  }

  validateConfirmPassword() {
    return (control: AbstractControl) => {
      if (!(control.value && this.resetPasswordForm.value.password)) {
        return { isSame: false };
      }
      return null;
    };
  }

  onSubmit() {
    if (this.resetPasswordForm.valid && this.resetPasswordForm.dirty) {
      this.authService
        .resetPassword(
          this.resetPasswordForm.get('password')?.value!,
          this.token!
        )
        .subscribe({
          next: () => {
            this.router.navigateByUrl('/login');
          },
          error: (err) => {
            this.toast.open('Token inválido', undefined, {
              panelClass: ['toast-error'],
              duration: 3 * 1000,
            });
          },
        });
    }
  }
}
