import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PasswordResetComponent } from './password-reset.component';
import { passwordResetGuard } from '../guards/password-reset.guard';

const routes: Routes = [
  {
    path: '',
    component: PasswordResetComponent,
    canActivate: [passwordResetGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordResetRoutingModule {}
