import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(private authService: AuthService, private router: Router) {}

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  getErrorMessage() {
    if (this.loginForm.get('email')?.hasError('required')) {
      return 'Introduza um email';
    }

    return this.loginForm.get('email')?.hasError('email')
      ? 'Email inválido'
      : '';
  }

  onSubmit() {
    if (this.loginForm.valid && this.loginForm.dirty) {
      this.authService
        .login(
          this.loginForm.value.email as string,
          this.loginForm.value.password as string
        )
        .subscribe({
          next: () => {
            this.router.navigateByUrl('protected');
          },
          error: (err: HttpErrorResponse) => {
            console.error(err);
            if (err.error.email) {
              this.loginForm
                .get('email')
                ?.setErrors({ error: err.error.email });
            }

            if (err.error.password) {
              this.loginForm
                .get('password')
                ?.setErrors({ error: err.error.password });
            }
          },
        });
    }
  }
}
