import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProtectedComponent } from './protected.component';
import { AuthGuard } from '../guards/auth.guard';
import { groupGuard, groupGuardChild } from '../guards/group.guard';

const routes: Routes = [
  {
    path: '',
    component: ProtectedComponent,
    canActivate: [AuthGuard, groupGuard],
    data: {
      group: [1, 999],
    },
    canActivateChild: [AuthGuard, groupGuardChild],
    children: [
      {
        path: 'users',
        data: {
          group: [999],
        },
        loadChildren: () =>
          import('../users/users.module').then((m) => m.UsersModule),
      },
      {
        path: 'clientes',
        data: {
          group: [999],
        },
        loadChildren: () =>
          import('../clientes/clientes.module').then((m) => m.ClientesModule),
      },
      {
        path: 'produtos',
        data: {
          group: [999],
        },
        loadChildren: () =>
          import('../produtos/produtos.module').then((m) => m.ProdutosModule),
      },
      {
        path: 'vendas',
        data: {
          group: [999],
        },
        loadChildren: () =>
          import('../vendas/vendas.module').then((m) => m.VendasModule),
      },
      {
        path: 'faltas',
        data: {
          group: [1, 999],
        },
        loadChildren: () =>
          import('../faltas/faltas.module').then((m) => m.FaltasModule),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProtectedRoutingModule {}
