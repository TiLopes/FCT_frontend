import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { catchError, map, of } from 'rxjs';

export const passwordResetGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const http = inject(HttpClient);
  const token = route.paramMap.get('token');

  return http.get('http://localhost:3000/api/reset/password/' + token).pipe(
    map((res) => {
      if (res.hasOwnProperty('error')) {
        router.navigate(['/login']);
        return false;
      }

      return true;
    }),
    catchError((e) => {
      console.log(e);
      router.navigate(['/login']);
      return of(false);
    })
  );
};
