import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { CanActivateChildFn, CanActivateFn, Router } from '@angular/router';

export const groupGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const http = inject(HttpClient);

  if (route.data['group'].includes(Number(localStorage.getItem('group')))) {
    http
      .post('http://localhost:3000/api/verify/auth', {
        group: Number(localStorage.getItem('group')),
      })
      .subscribe((res) => console.log(res));
    return true;
  }
  router.navigateByUrl('/login');
  return false;
};

export const groupGuardChild: CanActivateChildFn = (route, state) => {
  const router = inject(Router);

  if (route.data['group'].includes(Number(localStorage.getItem('group')))) {
    return true;
  }

  router.navigate(['protected']);
  return false;
};
