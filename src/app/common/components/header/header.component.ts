import { Component, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'protected-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  group: number;
  constructor(private authService: AuthService) {
    this.group = Number(localStorage.getItem('group'));
  }

  logout() {
    this.authService.logout();
  }
}
