import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaltasComponent } from './faltas.component';

const routes: Routes = [
  { path: '', component: FaltasComponent, pathMatch: 'full' },
  {
    path: 'marcar',
    loadChildren: () =>
      import('./criar-faltas/criar-faltas.module').then(
        (m) => m.CriarFaltasModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaltasRoutingModule {}
