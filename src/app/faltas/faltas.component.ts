import { CommonModule } from '@angular/common';
import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import {
  addDays,
  addHours,
  endOfDay,
  endOfMonth,
  isSameDay,
  isSameMonth,
  startOfDay,
  subDays,
} from 'date-fns';
import { Subject } from 'rxjs';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { CalendarOptions, EventSourceInput } from '@fullcalendar/core';
import { MatButtonModule } from '@angular/material/button';
import { FaltasService } from '../services/faltas.service';

@Component({
  selector: 'app-faltas',
  templateUrl: './faltas.component.html',
  styleUrls: ['./faltas.component.scss'],
})
export class FaltasComponent implements OnInit {
  startDate: Date;
  endDate: Date;
  constructor(public dialog: MatDialog, private faltasService: FaltasService) {}

  dialogRef: MatDialogRef<DialogContentComponent>;

  ngOnInit(): void {}

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay',
    },
    eventClick: this.handleEventClick.bind(this),
    datesSet: this.fetchEvents.bind(this),
    events: [],
    locale: 'pt',
    buttonText: {
      week: 'semana',
      month: 'mês',
      day: 'dia',
      today: 'hoje',
    },
    // contentHeight: 800,
  };

  handleDateClick(arg: any) {
    alert('date click! ' + arg.dateStr);
    console.log(arg);
  }

  handleEventClick(arg: any) {
    this.openDialog({
      id: arg.event.id,
      user: arg.event.title,
      motivo: arg.event.extendedProps?.description,
      estado: arg.event.extendedProps?.estado,
      start: arg.event.start,
      end: arg.event.end,
    });
  }

  openDialog({ id, user, motivo, estado, start, end }: any): void {
    this.dialogRef = this.dialog.open(DialogContentComponent, {
      data: { id, user, motivo, estado, start, end },
    });

    this.dialogRef.beforeClosed().subscribe((res) => {
      if (!res) {
        return;
      }

      this.fetchEvents({ start: this.startDate, end: this.endDate });
    });
  }

  fetchEvents(event: any) {
    let faltas: any = [];

    this.startDate = event.start;
    this.endDate = event.end;
    console.log('a');

    this.faltasService.getFaltas$(event.start, event.end).subscribe({
      next: (data: { success: boolean; faltas: any[] }) => {
        for (const falta of data.faltas) {
          faltas.push({
            id: falta.id,
            title: String(falta.user),
            description: falta.motivo,
            estado: falta.estado,
            start: falta.data_inicio,
            end: falta.data_fim,
            allDay: falta.dia_completo ? true : false,
            backgroundColor: falta.estado === 'Injustificada' ? 'red' : 'green',
          });
        }

        this.calendarOptions.events = faltas;
      },
    });
  }
}

@Component({
  selector: 'app-dialog-content',
  template: `
    <h2 mat-dialog-title>Event action occurred</h2>
    <mat-dialog-content>
      <div>
        Utilizador:
        <pre>{{ data.user }}</pre>
      </div>
      <div>
        Motivo:
        <pre>{{ data.motivo }}</pre>
      </div>
      <div>
        Estado:
        <pre>{{ data.estado }}</pre>
      </div>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button (click)="onClose()">OK</button>
      <button mat-button (click)="justificar(data.id)">Justificar</button>
    </mat-dialog-actions>
  `,
  standalone: true,
  imports: [MatDialogModule, CommonModule, MatButtonModule],
})
export class DialogContentComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogContentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private faltasService: FaltasService
  ) {}

  onClose(): void {
    this.dialogRef.close(false);
  }

  justificar(id: number) {
    // this.fetchEvents(this.data);
    this.faltasService.justificarFaltas$(id).subscribe({
      next: (res) => {
        this.dialogRef.close(true);
      },
      error: (err) => {
        console.log(err);
        this.dialogRef.close(false);
      },
    });
  }
}
