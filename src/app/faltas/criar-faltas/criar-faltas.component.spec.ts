import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarFaltasComponent } from './criar-faltas.component';

describe('CriarFaltasComponent', () => {
  let component: CriarFaltasComponent;
  let fixture: ComponentFixture<CriarFaltasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarFaltasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CriarFaltasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
