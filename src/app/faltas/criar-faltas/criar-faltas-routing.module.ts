import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CriarFaltasComponent } from './criar-faltas.component';

const routes: Routes = [{ path: '', component: CriarFaltasComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriarFaltasRoutingModule { }
