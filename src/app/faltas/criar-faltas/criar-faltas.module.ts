import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriarFaltasRoutingModule } from './criar-faltas-routing.module';
import { CriarFaltasComponent } from './criar-faltas.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MtxDatetimepickerModule } from '@ng-matero/extensions/datetimepicker';
import { MtxNativeDatetimeModule } from '@ng-matero/extensions/core';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [CriarFaltasComponent],
  imports: [
    CommonModule,
    CriarFaltasRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    ReactiveFormsModule,
    MtxDatetimepickerModule,
    MtxNativeDatetimeModule,
    MatCheckboxModule,
  ],
})
export class CriarFaltasModule {}
