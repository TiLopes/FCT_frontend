import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MtxCalendarView,
  MtxDatetimepickerMode,
  MtxDatetimepickerType,
} from '@ng-matero/extensions/datetimepicker';
import { Users } from 'src/app/models/users';
import { ClientesService } from 'src/app/services/clientes.service';
import { FaltasService } from 'src/app/services/faltas.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-criar-faltas',
  templateUrl: './criar-faltas.component.html',
  styleUrls: ['./criar-faltas.component.scss'],
})
export class CriarFaltasComponent implements OnInit {
  marcarFaltaForm = new FormGroup({
    user: new FormControl(null, [Validators.required]),
    data_inicio: new FormControl(new Date(), [Validators.required]),
    data_fim: new FormControl(new Date(), [Validators.required]),
    dia_completo: new FormControl(false),
    motivo: new FormControl('', [Validators.required]),
  });

  type: MtxDatetimepickerType = 'datetime';
  mode: MtxDatetimepickerMode = 'portrait';
  startView: MtxCalendarView = 'month';
  multiYearSelector = false;
  touchUi = false;
  twelvehour = false;
  timeInterval = 1;
  timeInput = true;
  utilizadores: Users[] = [];

  constructor(
    private usersService: UsersService,
    private faltasService: FaltasService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.usersService.getAllBasic$().subscribe({
      next: (res) => {
        this.utilizadores = res.users;
      },
    });
  }

  onSubmit() {
    if (this.marcarFaltaForm.invalid) {
      return;
    }

    this.faltasService
      .marcarFalta$({
        id_utilizador: this.marcarFaltaForm.get('user')?.value!,
        data_fim: this.marcarFaltaForm.get('data_fim')?.value!,
        data_inicio: this.marcarFaltaForm.get('data_inicio')?.value!,
        dia_completo: this.marcarFaltaForm.get('dia_completo')?.value!,
        motivo: this.marcarFaltaForm.get('motivo')?.value!,
      })
      .subscribe({
        next: (res) => {
          this.router.navigate(['..'], { relativeTo: this.route });
          this.snackBar.open('Marcada com sucesso', undefined, {
            panelClass: ['snackbar-success', 'centered'],
            duration: 3 * 1000,
          });
        },
      });
  }
}
