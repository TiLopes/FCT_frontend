import { Component, ViewChild, OnInit } from '@angular/core';
import { Users, UsersTable } from '../models/users';
import { MatTableDataSource } from '@angular/material/table';
import { UsersService } from '../services/users.service';
import { MatPaginator } from '@angular/material/paginator';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { merge, Observable, of, pipe } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nome', 'email', 'nif', 'group'];

  empTable: UsersTable;

  totalData: number;
  UserData: Users[];
  pageSizes = [5, 10];
  dataSource = new MatTableDataSource<Users>();

  isLoading = false;

  constructor(private usersService: UsersService, private http: HttpClient) {}

  @ViewChild('paginator') paginator: MatPaginator;

  getTableData(pageNumber: number, pageSize: number) {
    return this.usersService.getUsers(pageNumber, pageSize);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;

    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.getTableData(
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
          ).pipe(catchError(() => of(null)));
        }),
        map((userData) => {
          if (userData == null) return [];
          this.totalData = userData.total;
          this.isLoading = false;

          return userData.data;
        })
      )
      .subscribe((userData) => {
        this.UserData = userData;
        console.log(this.UserData);

        this.dataSource = new MatTableDataSource(this.UserData);
      });
  }

  // * Apenas para debug
  logClickedRow(row: Users) {
    console.log(row);
  }

  ngOnInit(): void {
    this.isLoading = true;
  }
}
