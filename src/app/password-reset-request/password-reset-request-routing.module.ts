import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PasswordResetRequestComponent } from './password-reset-request.component';

const routes: Routes = [
  { path: '', component: PasswordResetRequestComponent },
  {
    path: ':token',
    loadChildren: () =>
      import('../password-reset/password-reset.module').then(
        (m) => m.PasswordResetModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordResetRequestRoutingModule {}
