import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset-request.component.html',
  styleUrls: ['./password-reset-request.component.scss'],
})
export class PasswordResetRequestComponent {
  resetForm = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
  });

  toastDuration: number = 4;

  constructor(
    private router: Router,
    private toast: MatSnackBar,
    private authService: AuthService
  ) {}

  onSubmit() {
    if (this.resetForm.invalid) {
      return;
    }

    this.authService
      .resetPasswordRequest(this.resetForm.get('email')?.value!)
      .subscribe((res) => {
        this.toast.open('Deve receber um email dentro de momentos', '', {
          duration: this.toastDuration * 1000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
        });

        console.log(res);
      });
  }
}
