import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriarVendaRoutingModule } from './criar-venda-routing.module';
import { CriarVendaComponent } from './criar-venda.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MtxDatetimepickerModule } from '@ng-matero/extensions/datetimepicker';
import { MtxNativeDatetimeModule } from '@ng-matero/extensions/core';
import { MatSelectModule } from '@angular/material/select';
import {
  NgxCurrencyDirective,
  NgxCurrencyInputMode,
  provideEnvironmentNgxCurrency,
} from 'ngx-currency';

@NgModule({
  declarations: [CriarVendaComponent],
  providers: [
    provideEnvironmentNgxCurrency({
      align: 'left',
      // allowNegative: true,
      allowZero: true,
      decimal: ',',
      precision: 2,
      prefix: '',
      suffix: ' €',
      thousands: '.',
      nullable: false,
      min: null,
      max: null,
      inputMode: NgxCurrencyInputMode.Financial,
    }),
  ],
  imports: [
    CommonModule,
    CriarVendaRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MtxDatetimepickerModule,
    MtxNativeDatetimeModule,
    MatSelectModule,
    ReactiveFormsModule,
    NgxCurrencyDirective,
  ],
})
export class CriarVendaModule {}
