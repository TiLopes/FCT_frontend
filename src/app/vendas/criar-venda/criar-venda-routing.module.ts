import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CriarVendaComponent } from './criar-venda.component';

const routes: Routes = [{ path: '', component: CriarVendaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriarVendaRoutingModule { }
