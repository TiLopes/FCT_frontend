import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  MtxCalendarView,
  MtxDatetimepickerMode,
  MtxDatetimepickerType,
} from '@ng-matero/extensions/datetimepicker';
import { Clientes } from 'src/app/models/clientes';
import { Produtos } from 'src/app/models/produtos';
import { ClientesService } from 'src/app/services/clientes.service';
import { ProdutosService } from 'src/app/services/produtos.service';
import { VendasService } from 'src/app/services/vendas.service';

@Component({
  selector: 'app-criar-venda',
  templateUrl: './criar-venda.component.html',
  styleUrls: ['./criar-venda.component.scss'],
})
export class CriarVendaComponent implements OnInit {
  criarVendaForm = new FormGroup({
    preco: new FormControl(0.0, [Validators.required]),
    data_venda: new UntypedFormControl(new Date()),
    produtos: new FormArray([
      new FormGroup({
        produto: new FormControl(null, [Validators.required]),
        quantidade: new FormControl(1, [Validators.required]),
      }),
    ]),
    cliente: new FormControl(null, [Validators.required]),
  });

  type: MtxDatetimepickerType = 'datetime';
  mode: MtxDatetimepickerMode = 'portrait';
  startView: MtxCalendarView = 'month';
  multiYearSelector = false;
  touchUi = false;
  twelvehour = false;
  timeInterval = 1;
  timeInput = true;
  produtosDisponiveis: Produtos[] = [];
  clientes: Clientes[] = [];

  constructor(
    private vendasService: VendasService,
    private produtosService: ProdutosService,
    private clientesService: ClientesService,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.produtosService.getAll().subscribe({
      next: (data) => {
        this.produtosDisponiveis = data.produtos;
        console.log(this.produtosDisponiveis);
      },
    });

    this.clientesService.getAllBasic$().subscribe({
      next: (data) => {
        this.clientes = data.clientes;
        console.log(this.clientes);
      },
    });
  }

  get produtos() {
    return this.criarVendaForm.get('produtos') as FormArray;
  }

  addProduto() {
    this.produtos.push(
      new FormGroup({
        produto: new FormControl(null, [Validators.required]),
        quantidade: new FormControl(1, [Validators.required]),
      })
    );
  }

  calcularPreco() {
    this.criarVendaForm
      .get('produtos')
      ?.valueChanges.subscribe((produtos: any[]) => {
        let preco = 0;
        produtos.forEach((prod) => {
          // calculate preco based on selected option
          // for example, if the option value is the price:
          preco += prod.produto?.preco * prod.quantidade;
        });
        this.criarVendaForm.get('preco')?.setValue(preco);
        console.log(this.criarVendaForm.get('preco')?.value);
      });
  }

  onSubmit() {
    if (this.criarVendaForm.invalid) {
      return;
    }

    let produtosFormatados: { id: number; quantidade: number }[] = [];

    for (const produto of this.produtos.value) {
      produtosFormatados.push({
        id: Number(produto.produto.id),
        quantidade: Number(produto.quantidade),
      });
    }

    this.vendasService
      .addVenda({
        cliente: Number(this.criarVendaForm.get('cliente')!.value),
        data_venda: this.criarVendaForm.get('data_venda')!.value,
        preco: Number(this.criarVendaForm.get('preco')!.value),
        produtos: produtosFormatados,
      })
      .subscribe({
        next: (res) => {
          console.log(res);
        },
      });
  }
}
