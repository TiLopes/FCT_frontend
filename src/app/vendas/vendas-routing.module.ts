import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendasComponent } from './vendas.component';

const routes: Routes = [
  { path: '', component: VendasComponent },
  {
    path: 'criar',
    loadChildren: () =>
      import('../vendas/criar-venda/criar-venda.module').then(
        (m) => m.CriarVendaModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendasRoutingModule {}
