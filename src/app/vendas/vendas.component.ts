import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {
  catchError,
  map,
  startWith,
  switchMap,
  mergeWith,
} from 'rxjs/operators';
import { of, pipe } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../common/components/confirmation-dialog/confirmation-dialog.component';
import { Vendas, VendasTable } from '../models/vendas';
import { VendasService } from '../services/vendas.service';

@Component({
  selector: 'app-vendas',
  templateUrl: './vendas.component.html',
  styleUrls: ['./vendas.component.scss'],
})
export class VendasComponent {
  displayedColumns: string[] = [
    'id',
    'data_venda',
    'produtos',
    'cliente',
    'preco',
    'acoes',
  ];

  empTable: VendasTable;

  totalData: number;
  VendasData: Vendas[];
  pageSizes = [5, 10];
  dataSource = new MatTableDataSource<Vendas>();
  isLoading = false;

  constructor(
    private produtosService: VendasService,
    public dialog: MatDialog
  ) {}

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    this.sort.sortChange
      .pipe(
        mergeWith(this.paginator.page),
        startWith({}),
        switchMap(() => {
          this.isLoading = true;
          return this.produtosService
            .getVendas(
              this.paginator.pageIndex + 1,
              this.paginator.pageSize,
              this.sort.active,
              this.sort.direction
            )
            .pipe(catchError(() => of(null)));
        }),
        map((data) => {
          // Flip flag to show that loading has finished.
          this.isLoading = false;
          if (data === null) {
            return [];
          }
          // Only refresh the result length if there is new data. In case of rate
          // limit errors, we do not want to reset the paginator to zero, as that
          // would prevent users from re-triggering requests.
          this.totalData = data.total;
          return data.data;
        })
      )
      .subscribe((data) => {
        this.VendasData = data;

        this.dataSource = new MatTableDataSource(this.VendasData);
      });
  }

  // * Apenas para debug
  logClickedRow(row: Vendas) {
    console.log(row);
  }

  ngOnInit(): void {
    this.isLoading = true;
  }

  openConfirmationDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: { message: 'Tem certeza que deseja remover este produto?' },
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      console.log('The dialog was closed');
      console.log(result);
    });
  }
}
