export interface Produtos {
  id: number;
  nome: string;
  preco: number;
}

export interface ProdutosTable {
  data: Produtos[];
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
}
