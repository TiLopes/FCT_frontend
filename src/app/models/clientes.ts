export interface Clientes {
  id: number;
  nome: string;
  email: string;
  telefone: string;
  nif: number;
  cod_postal: string;
  morada: string;
}

export interface ClientesTable {
  data: Clientes[];
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
}
