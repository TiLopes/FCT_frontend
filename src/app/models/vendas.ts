import { Clientes } from './clientes';
import { Produtos } from './produtos';

export interface Vendas {
  id: number;
  data_venda: string;
  cliente: Clientes;
  preco: number;
  produtos: Produtos[];
}

export interface VendasProdutos {
  id_venda?: number;
  id_produto?: number;
  quantidade?: number;
}

export interface VendasTable {
  data: Vendas[];
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
}
