export interface Users {
  id: number;
  nome: string;
  email: string;
  nif: string;
  group: {
    id: number;
    nome: string;
  };
}

export interface UsersTable {
  data: Users[];
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
}
